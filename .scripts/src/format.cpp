#include <format>
#include <print>

int main(int argc, char** argv) {
  if (argc < 2 || argc > 6) {
    std::println("Usage: {} <text> <width> [<fillchar>] [<location>] [<newline>:y]", argv[0]);
    return 1;
  }

  char* text     = argv[1];
  char* width    = argv[2];
  char  filler   = argc >= 4 ? argv[3][0] : ' ';
  char  location = argc >= 5 ? argv[4][0] : '<';
  bool  newline  = argc >= 6 ? argv[5][0] == 'y' : false;

  auto format = std::format("{{:{}{}{}}}", filler, location, width);

  if (newline) {
    std::println(std::runtime_format(format), text, width, filler, location);
  } else {
    std::print(std::runtime_format(format), text, width, filler, location);
  }
}
