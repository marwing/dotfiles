#!/bin/sh

command_exists() {
  command -v "$1" > /dev/null 2>&1
}

wait_online() {
  while ! ping -c 1 1.1.1.1 > /dev/null 2>&1; do sleep 5; done
}

get_updates_count_fwupd() {
  if command_exists fwupdmgr; then
    fwupdmgr --json get-updates | jq '.Devices | length'
  fi
}

get_updates_count_pacman() {
  if command_exists checkupdates; then
    checkupdates | wc -l
  elif command_exists pacman; then
    pacman -Qu | grep -v "\[ignored\]" | wc -l
  fi
}

get_updates_count_aur() {
  if command_exists paru; then
    paru -Qua | grep -v "\[ignored\]" | wc -l
  fi
}

wait_online

FWUPD_UPDATE_COUNT=$(get_updates_count_fwupd)
PACMAN_UPDATE_COUNT=$(get_updates_count_pacman)
AUR_UPDATE_COUNT=$(get_updates_count_aur)

echo "{ \"text\": \"$((${FWUPD_UPDATE_COUNT:-0} + ${PACMAN_UPDATE_COUNT:-0} + ${AUR_UPDATE_COUNT:-0}))\", \"tooltip\": \"fwupd: ${FWUPD_UPDATE_COUNT:-error}\\npacman: ${PACMAN_UPDATE_COUNT:-error}\\nAUR: ${AUR_UPDATE_COUNT:-error}\" }"
