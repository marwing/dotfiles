path_prepend() {
  export PATH="$1:$PATH"
}

if (($+commands[yarn])); then
  path_prepend "$(yarn --offline global bin)"
fi

path_prepend "$HOME/.cargo/bin"
path_prepend "$HOME/.local/bin"
path_prepend "$HOME/.scripts"

# export some variables
export EDITOR="nvim"
export VISUAL="nvim"
export BROWSER="firefox"
export PAGER="less"
export MANPAGER="nvim +Man! '+set signcolumn=no'"
export TERMINAL="alacritty"
export PDFVIEWER="zathura" # texdoc

# enable wayland
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1
export _JAVA_AWT_WM_NONREPARENTING=1

export RANGER_LOAD_DEFAULT_RC=false

export DVDCSS_CACHE="${XDG_DATA_HOME:-$HOME/.local/share}"/dvdcss

export MAKEFLAGS=-j$(($(nproc) + 2))

export CMAKE_COLOR_DIAGNOSTICS=ON

# use ccache in cmake builds if available
if (($+commands[ccache])); then
  for lang in "C" "CXX"; do
    export CMAKE_${lang}_COMPILER_LAUNCHER="ccache"
  done
fi

# use ninja for cmake builds if available
if (($+commands[ninja])); then
  export CMAKE_GENERATOR="Ninja"
fi

export CPM_SOURCE_CACHE=$HOME/.cache/cpm

[ "$(tty)" = "/dev/tty1" ] && (($+commands[sway])) && exec systemd-cat --identifier=sway sway
