if (( $+commands[ykman] )); then
  eval "$(_YKMAN_COMPLETE=zsh_source ykman)"
fi

if (( $+commands[yq] )); then
  eval "$(register-python-argcomplete yq)"
fi

if (( $+commands[pipx] )); then
  eval "$(register-python-argcomplete pipx)"
fi
