alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias ip='ip --color=auto'
alias dfs='dotfiles'
alias ls='exa'
alias tf='terraform'
alias rip='pv /dev/sr0 > $(vobcopy -I /dev/sr0 2>&1 | awk "/DVD-name/ {print \$3}").iso && eject'
alias ffmpeg="ffmpeg -hide_banner"
alias pall='for r in $(find -mindepth 1 -maxdepth 1 -type d); do format $(basename "${r}") 60 - ^ y; git -C "${r}" pull; done'
alias purgeemptydirs='find . -depth -type d -empty -delete'
alias arcsize='arc_summary | grep "ARC size"'
