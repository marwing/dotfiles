local overrides = {
  capabilities = (function()
    local capabilities = vim.lsp.protocol.make_client_capabilities()

    if pcall(require, 'cmp_nvim_lsp') then
      capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())
    end

    return capabilities
  end)(),
}

overrides.default_params = setmetatable({
  capabilities = overrides.capabilities,
}, {
  __call = function(table, opts)
    return vim.tbl_deep_extend('force', table, opts or {})
  end,
})

return overrides
