local function disable_for(clients)
  return function(client)
    return not vim.tbl_contains(clients, client.name)
  end
end

local function on_list_trouble(list)
  vim.fn.setqflist({}, ' ', list)
  require('trouble').open { mode = 'qflist' }
end

-- :h *lsp-defaults-disable*
require('user.utils.lsp').on_attach('lsp defaults disable', function(_, bufnr)
  vim.bo[bufnr].formatexpr = nil
  vim.bo[bufnr].omnifunc = nil
end)

require('user.utils.lsp').on_attach('lsp keybinds', function(client, bufnr)
  local function set(key, action, method_or_client, modes)
    if
      not method_or_client
      or (string.match(method_or_client, '%a/%a') and client:supports_method(method_or_client))
      or client.name == method_or_client
    then
      vim.keymap.set(modes or 'n', key, action, { buffer = bufnr })
    end
  end

  set('gD', function()
    vim.lsp.buf.declaration { on_list = on_list_trouble }
  end, 'textDocument/declaration')

  set('gd', function()
    vim.lsp.buf.definition { on_list = on_list_trouble }
  end, 'textDocument/definition')

  set('gt', function()
    vim.lsp.buf.type_definition { on_list = on_list_trouble }
  end)

  set('grr', function()
    vim.lsp.buf.references(nil, { on_list = on_list_trouble })
  end)

  set('gri', function()
    vim.lsp.buf.implementation { on_list = on_list_trouble }
  end)

  set('<C-k>', vim.lsp.buf.signature_help)

  set('<A-S-f>', function()
    vim.lsp.buf.format {
      filter = disable_for { 'lua_ls', 'texlab', 'basedpyright' },
    }
  end, nil, { 'n', 'v' })

  set('<space>wa', vim.lsp.buf.add_workspace_folder)
  set('<space>wr', vim.lsp.buf.remove_workspace_folder)
  set('<space>wl', function()
    vim.schedule(function()
      vim.notify(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end)
  end)

  -- server extensions
  set('<F12>', '<cmd>ClangdSwitchSourceHeader<CR>', 'clangd')
end)
