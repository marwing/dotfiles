vim.diagnostic.config {
  update_in_insert = true,
  severity_sort = true,
  virtual_text = { source = true },
  float = { source = true },
}

vim.keymap.set('n', 'dD', vim.diagnostic.open_float)
vim.keymap.set('n', 'dq', vim.diagnostic.setloclist)
vim.keymap.set('n', '<leader>dd', function()
  vim.diagnostic.enable(false, { bufnr = 0 })
end)
vim.keymap.set('n', '<leader>de', function()
  vim.diagnostic.enable(true, { bufnr = 0 })
end)
