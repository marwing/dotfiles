vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- Shortcutting split navigation:
vim.keymap.set({ 'n', 'v', 't' }, '<A-h>', '<Cmd>wincmd h<CR>')
vim.keymap.set({ 'n', 'v', 't' }, '<A-j>', '<Cmd>wincmd j<CR>')
vim.keymap.set({ 'n', 'v', 't' }, '<A-k>', '<Cmd>wincmd k<CR>')
vim.keymap.set({ 'n', 'v', 't' }, '<A-l>', '<Cmd>wincmd l<CR>')

-- Shortcutting tab navigation
vim.keymap.set('n', '<leader>n', '<cmd>tabprevious<cr>')
vim.keymap.set('n', '<leader>m', '<cmd>tabnext<cr>')

-- escaping from :terminal
vim.keymap.set('t', '<Esc><Esc>', [[<C-\><C-n>]])

vim.keymap.set('n', '<leader>Bu', '<cmd>bunload<cr>')
vim.keymap.set('n', '<leader>Bd', '<cmd>bdelete<cr>')
vim.keymap.set('n', '<leader>Bw', '<cmd>bwipeout<cr>')

local function toggle_checkbox()
  local line = vim.api.nvim_win_get_cursor(0)[1]
  local text = vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]

  local prefix, mark, postfix = string.match(text, '^(%s*[-*+]%s+%[)([ xX])(%].*)$')
  if prefix == nil then
    vim.notify('No checkbox found')
    return
  end

  local new_mark = 'x'
  if vim.tbl_contains({ 'x', 'X' }, mark) then
    new_mark = ' '
  end

  local new_text = string.format('%s%s%s', prefix, new_mark, postfix)

  vim.api.nvim_buf_set_lines(0, line - 1, line, true, { new_text })
end

vim.keymap.set('n', '<Space>tc', toggle_checkbox)

require('user.lsp.mappings')
