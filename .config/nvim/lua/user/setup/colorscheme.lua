return {
  {
    'ellisonleao/gruvbox.nvim',
    priority = 1000,
    opts = {
      italic = {
        strings = false,
      },
      overrides = {
        DiagnosticFloatingWarn = { link = 'DiagnosticWarn' },
        ['@variable'] = { link = 'Identifier' },

        -- overrides for vim.lsp.buf.document_highlight()
        LspReferenceText = { link = 'CursorLine' },
        LspReferenceWrite = { link = 'CursorLine' },
        LspReferenceRead = { link = 'CursorLine' },

        DiffDelete = { fg = 'NONE', bg = '#431313', reverse = false },
        DiffAdd = { fg = 'NONE', bg = '#142a03', reverse = false },
        DiffChange = { fg = 'NONE', bg = '#3b3307', reverse = false },
        DiffText = { fg = 'NONE', bg = '#4d520d', reverse = false },

        GitSignsAdd = { link = 'GruvboxGreenSign' },
        GitSignsChange = { link = 'GruvboxAquaSign' },
        GitSignsDelete = { link = 'GruvboxRedSign' },

        FlashLabel = { bg = '#9d0006' }, -- Gruvbox palette faded_orange

        LazyDimmed = { link = 'Comment' },

        -- workaround for broken heirline highlights because of neovim merging
        -- higlight groups in statusline, tabline and winbar with user
        -- specified ones since https://github.com/neovim/neovim/pull/29976
        StatusLine = { fg = '#ebdbb2', bg = '#504945', reverse = false },
        StatusLineNC = { link = 'StatusLine' },
        WinBarNC = { link = 'WinBar' },
      },
    },
    config = function(_, opts)
      require('gruvbox').setup(opts)
      vim.cmd.colorscheme('gruvbox')
    end,
  },
  {
    'folke/tokyonight.nvim',
    opts = {
      style = 'moon',
      sidebars = {
        'help',
        'qf',
        'tagbar',
      },
    },
    lazy = true,
  },
}
