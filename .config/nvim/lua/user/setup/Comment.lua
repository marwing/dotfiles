return {
  'numToStr/Comment.nvim',
  opts = {
    ignore = '^$',
  },
  keys = {
    { 'gc', mode = { 'n', 'x', 'v' }, desc = 'Comment', nowait = true },
  },
}
