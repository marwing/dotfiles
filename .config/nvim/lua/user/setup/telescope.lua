local function trouble_open(...)
  require('trouble.sources.telescope').open(...)
end

local function trouble_add(...)
  require('trouble.sources.telescope').add(...)
end

return {
  'nvim-telescope/telescope.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make' },
  },
  config = function()
    local telescope = require('telescope')
    telescope.load_extension('fzf')

    -- nvim-notify loads it's telescope extension automatically when it is first used after telescope
    -- this is only necessary to load the extension if telescope is loaded after nvim-notify
    if package.loaded['notify'] then
      telescope.load_extension('notify')
    end

    telescope.setup {
      defaults = {
        mappings = {
          i = {
            ['<c-q>'] = trouble_open,
            ['<c-a>'] = trouble_add,
          },
          n = {
            ['<c-q>'] = trouble_open,
            ['<c-a>'] = trouble_add,
          },
        },
      },
    }
  end,
  -- stylua: ignore
  keys = {
    { '<leader>fb', function() require('telescope.builtin').builtin() end,                        desc = 'Telescope builtin' },
    { '<leader>fr', function() require('telescope.builtin').resume() end,                         desc = 'Telescope resume' },
    { '<leader>ff', function() require('telescope.builtin').find_files() end,                     desc = 'Telescope find_files' },
    { '<leader>fl', function() require('telescope.builtin').buffers { sort_lastused = true } end, desc = 'Telescope buffers' },
    { '<leader>fg', function() require('telescope.builtin').live_grep() end,                      desc = 'Telescope live_grep' },
    { '<leader>fo', function() require('telescope.builtin').oldfiles() end,                       desc = 'Telescope old_files' },
  },
}
