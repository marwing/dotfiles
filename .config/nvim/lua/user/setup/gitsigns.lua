return {
  'lewis6991/gitsigns.nvim',
  opts = {
    worktrees = {
      {
        toplevel = vim.env.HOME,
        gitdir = vim.env.HOME .. '/.dotfiles',
      },
    },
    on_attach = function(bufnr)
      local gs = require('gitsigns')

      -- Navigation
      vim.keymap.set('n', ']c', function()
        if vim.wo.diff then
          vim.cmd.normal { ']c', bang = true }
        end
        gs.nav_hunk('next')
        return '<Ignore>'
      end, { expr = true, buffer = bufnr })

      vim.keymap.set('n', '[c', function()
        if vim.wo.diff then
          vim.cmd.normal { '[c', bang = true }
        end
        gs.nav_hunk('prev')
        return '<Ignore>'
      end, { expr = true, buffer = bufnr })

      -- Text object
      vim.keymap.set({ 'o', 'x' }, 'ih', ':<C-U>Gitsigns select_hunk<CR>', { buffer = bufnr })

      -- Actions
      vim.keymap.set('n', '<leader>gb', gs.blame_line, { buffer = bufnr, desc = 'Gitsigns blame_line' })
      vim.keymap.set('n', '<leader>gB', gs.blame, { buffer = bufnr, desc = 'Gitsigns blame' })
      vim.keymap.set('n', '<leader>gp', gs.preview_hunk, { buffer = bufnr, desc = 'Gitsigns preview_hunk' })
      vim.keymap.set('n', '<leader>gi', gs.preview_hunk_inline, { buffer = bufnr, desc = 'Gitsigns preview_hunk_inline' })

      vim.keymap.set('n', '<leader>gd', gs.diffthis, { buffer = bufnr, desc = 'Gitsigns diffthis' })

      vim.keymap.set('n', '<leader>gsb', gs.stage_buffer, { buffer = bufnr, desc = 'Gitsigns stage_buffer' })
      vim.keymap.set('n', '<leader>gsh', gs.stage_hunk, { buffer = bufnr, desc = 'Gitsigns stage_hunk' })
      vim.keymap.set('v', '<leader>gsh', function()
        gs.stage_hunk { vim.fn.line('.'), vim.fn.line('v') }
      end, { buffer = bufnr, desc = 'Gitsigns stage_hunk' })
      vim.keymap.set('n', '<leader>gsu', gs.undo_stage_hunk, { buffer = bufnr, desc = 'Gitsigns undo_stage_hunk' })

      vim.keymap.set('n', '<leader>grb', gs.reset_buffer, { buffer = bufnr, desc = 'Gitsigns reset_buffer' })
      vim.keymap.set('n', '<leader>grh', gs.reset_hunk, { buffer = bufnr, desc = 'Gitsigns reset_hunk' })
      vim.keymap.set('v', '<leader>grh', function()
        gs.reset_hunk { vim.fn.line('.'), vim.fn.line('v') }
      end)
    end,
  },
  event = 'VeryLazy',
}
