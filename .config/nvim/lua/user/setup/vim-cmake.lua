local cmdline_cache = {}

local function prompt_cmdline(target)
  vim.ui.input({}, function(cmdline)
    if cmdline == nil then
      return
    end
    if cmdline ~= '' then
      if not vim.list_contains(cmdline_cache, cmdline) then
        vim.list_extend(cmdline_cache, { cmdline })
        table.sort(cmdline_cache)
      end
    end
    vim.fn['cmake#Run'](target, unpack(vim.split(cmdline, ' ')))
  end)
end

---@param with_cmdline boolean
local function cmake_run_prompt(with_cmdline)
  local targets = vim.split(vim.fn['cmake#GetExecTargets']('', '', 0), '\n')
  vim.ui.select(targets, {}, function(target)
    if target == nil then
      return
    end
    if not with_cmdline then
      vim.fn['cmake#Run'](target)
    else
      if #cmdline_cache == 0 then
        prompt_cmdline(target)
      else
        vim.ui.select(vim.list_extend({ ' :: new' }, cmdline_cache), {}, function(cmdline)
          if cmdline == nil then
            return
          end
          if cmdline == ' :: new' then
            prompt_cmdline(target)
          else
            vim.fn['cmake#Run'](target, unpack(vim.split(cmdline, ' ')))
          end
        end)
      end
    end
  end)
end

local function cmake_build_prompt()
  local targets = vim.split(vim.fn['cmake#GetBuildTargets']('', '', 0), '\n')
  table.sort(targets)
  vim.ui.select(targets, {}, function(target)
    if target == nil then
      return
    end
    vim.fn['cmake#Build'](false, target)
  end)
end

-- TODO: add a mechanism to select a target to be built by <leader>cb and display it in the statusline

return {
  'cdelledonne/vim-cmake',
  init = function()
    vim.g.cmake_build_dir_location = 'build'
    vim.g.cmake_link_compile_commands = 1
    vim.g.cmake_generate_options = { '-GNinja', '-DENABLE_DEVELOPER_MODE:BOOL=ON' }
  end,
  -- stylua: ignore
  keys = {
    { '<leader>cg',  '<Plug>(CMakeGenerate)',                desc = 'CMake generate' },
    { '<leader>cb',  '<Plug>(CMakeBuild)',                   desc = 'CMake build' },
    { '<leader>cB',  cmake_build_prompt,                     desc = 'CMake build' },
    { '<leader>ct',  '<Plug>(CMakeTest)',                    desc = 'CMake test' },
    { '<leader>cr',  function() cmake_run_prompt(true) end,  desc = 'Pick and run cmake target (prompt for cmdline)' },
    { '<leader>cR',  function() cmake_run_prompt(false) end, desc = 'Pick and run cmake target (no cmdline)' },
    { '<leader>csr', '<cmd>CMakeSwitch Release<CR>',         desc = 'CMake switch Release' },
    { '<leader>csd', '<cmd>CMakeSwitch Debug<CR>',           desc = 'CMake switch Debug' },
    { '<leader>cq',  '<Plug>(CMakeClose)',                   desc = 'CMake close' },
  },
  cmd = { 'CMakeGenerate', 'CMakeClean', 'CMakeRun', 'CMakeBuild' },
}
