return {
  {
    -- TODO: review (swayconfig in core)
    'terminalnode/sway-vim-syntax',
    ft = 'swayconfig',
  },
  {
    'rhysd/committia.vim',
    config = function()
      vim.g.committia_hooks = {
        edit_open = function(info)
          vim.keymap.set({ 'n', 'i' }, '<C-f>', '<Plug>(committia-scroll-diff-down-half)', { buffer = true })
          vim.keymap.set({ 'n', 'i' }, '<C-b>', '<Plug>(committia-scroll-diff-up-half)', { buffer = true })

          local buf = vim.api.nvim_buf_get_lines(0, 0, -1, true)
          if info.vcs == 'git' and #buf == 1 and buf[1] == '' then
            vim.cmd.startinsert()
          end
        end,
      }
    end,
  },
  {
    'dstein64/vim-startuptime',
    cmd = 'StartupTime',
  },
  {
    'tpope/vim-repeat',
    event = 'VeryLazy',
  },
  {
    'tpope/vim-obsession',
    cmd = 'Obsession',
    event = 'SessionLoadPost',
  },
  {
    'famiu/bufdelete.nvim',
    cmd = { 'Bdelete', 'Bwipeout' },
    -- stylua: ignore
    keys = {
      { '<leader>bd', function() require'bufdelete'.bufdelete(0) end,  desc = 'bufdelete' },
      { '<leader>bw', function() require'bufdelete'.bufwipeout(0) end, desc = 'bufwipeout' },
    },
  },
  {
    'junegunn/vim-easy-align',
    keys = {
      { 'ga', '<Plug>(EasyAlign)', mode = { 'n', 'x' } },
    },
  },
  {
    'tpope/vim-surround',
    event = 'VeryLazy',
  },
  {
    'norcalli/nvim-colorizer.lua',
    config = function()
      require('colorizer').setup({}, {
        css = true,
      })
    end,
    cmd = { 'ColorizerToggle', 'ColorizerAttachToBuffer' },
  },
  {
    'dhruvasagar/vim-table-mode',
    cmd = 'TableModeToggle',
  },
  { 'direnv/direnv.vim' },
  {
    'sindrets/diffview.nvim',
    cmd = {
      'DiffviewOpen',
      'DiffviewRefresh',
      'DiffviewFocusFiles',
      'DiffviewFileHistory',
      'DiffviewToggleFiles',
    },
  },
  {
    'mbbill/undotree',
    cmd = 'UndotreeToggle',
  },
}
