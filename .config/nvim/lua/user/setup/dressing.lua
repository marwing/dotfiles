return {
  'stevearc/dressing.nvim',
  dependencies = 'MunifTanjim/nui.nvim',
  opts = {
    input = {
      start_in_insert = false,
    },
  },
  event = 'VeryLazy',
}
