return {
  'nvimtools/none-ls.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'gbprod/none-ls-shellcheck.nvim',
  },
  config = function()
    local null_ls = require('null-ls')
    null_ls.setup {
      on_attach = require('user.lsp.overrides').on_attach,
      sources = {
        -- git
        null_ls.builtins.code_actions.gitrebase,

        -- C++
        null_ls.builtins.diagnostics.cppcheck,

        -- Nix
        null_ls.builtins.diagnostics.deadnix,
        null_ls.builtins.diagnostics.statix,
        null_ls.builtins.code_actions.statix,

        -- shell
        require('none-ls-shellcheck.diagnostics'),
        require('none-ls-shellcheck.code_actions'),
        null_ls.builtins.diagnostics.zsh,
        null_ls.builtins.formatting.shfmt.with {
          extra_args = function(params)
            local opts = params.options or {}

            return {
              '--indent',
              opts.insertSpaces and opts.tabSize or 0,
              '--space-redirects',
              '--case-indent',
              '--binary-next-line',
            }
          end,
        },

        -- lua
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.diagnostics.selene.with {
          cwd = function()
            -- falls back to cwd if return is nil
            return vim.fs.dirname(vim.fs.find('selene.toml', { upward = true, path = vim.api.nvim_buf_get_name(0) })[1])
          end,
        },

        -- python
        null_ls.builtins.formatting.black,

        -- ansible
        null_ls.builtins.diagnostics.ansiblelint,

        -- github actions
        null_ls.builtins.diagnostics.actionlint
      },
    }
  end,
  event = 'VeryLazy',
}
