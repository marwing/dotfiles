return {
  {
    'folke/lazydev.nvim',
    opts = {
      library = {
        { path = '${3rd}/luv/library', words = { 'vim%.uv' } },
      },
    },
    ft = 'lua',
  },
  {
    'neovim/nvim-lspconfig',
    opts = {
      lua_ls = {
        settings = {
          Lua = {
            completion = {
              callSnippet = 'Replace',
            },
            telemetry = {
              enable = false,
            },
          },
        },
      },
    },
  },
}
