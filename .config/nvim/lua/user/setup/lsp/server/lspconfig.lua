return {
  {
    'neovim/nvim-lspconfig',
    opts = {
      cmake = {},
      ltex = {
        -- copied from lspconfig to disable gitcommit
        filetypes = {
          'bib',
          -- ltex uses a lot of memory and the commit message buffer is often closed long before it can load
          -- 'gitcommit',
          'markdown',
          'org',
          'plaintex',
          'rst',
          'rnoweb',
          'tex',
          'pandoc',
          'quarto',
          'rmd',
          'context',
          'html',
          'xhtml',
          'mail',
          'text',
        },
      },
      basedpyright = {},
      terraformls = {},
      texlab = {},
      nixd = {
        on_attach = function(client, bufnr)
          vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
        end,
        settings = {
          nixd = {
            formatting = {
              command = { 'alejandra' },
            },
          },
        },
      },
      ts_ls = {},
      vimls = {},
      jsonls = {
        on_new_config = function(new_config)
          new_config.settings.json.schemas = new_config.settings.json.schemas or {}
          vim.list_extend(new_config.settings.json.schemas, require('schemastore').json.schemas())
        end,
        settings = {
          json = {
            validate = { enable = true },
          },
        },
      },
      yamlls = {
        on_new_config = function(new_config)
          new_config.settings.yaml.schemas = new_config.settings.yaml.schemas or {}
          vim.list_extend(new_config.settings.yaml.schemas, require('schemastore').yaml.schemas())
        end,
        settings = {
          yaml = {
            validate = { enable = true },
          },
        },
      },
    },
    config = function(_, opts)
      local default_params = require('user.lsp.overrides').default_params

      for server, config in pairs(opts) do
        vim.validate {
          server = { server, 'string' },
          config = { config, 'table' },
        }

        require('lspconfig')[server].setup(default_params(config))
      end
    end,
    event = 'FileType',
  },
  {
    'b0o/schemastore.nvim',
    lazy = true,
  },
}
