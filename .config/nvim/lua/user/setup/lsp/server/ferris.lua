vim.api.nvim_create_autocmd('LspAttach', {
  pattern = '*.rs',
  callback = function(args)
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if client:supports_method("textDocument/inlayHint") then
      vim.lsp.inlay_hint.enable(true, { bufnr = args.bufnr })
    end
  end,
})

return {
  'vxpm/ferris.nvim',
  opts = {},
  dependencies = {
    'neovim/nvim-lspconfig',
    opts = {
      rust_analyzer = {
        settings = {
          ['rust-analyzer'] = {
            check = {
              command = 'clippy',
            },
            completion = {
              fullFunctionSignatures = {
                enable = true,
              },
            },
          },
        },
      },
    },
  },
  ft = 'rust',
}
