-- TODO: simplify with new heirline search cookbook (commit 673226cbbb4da6f595a78cc03b18682f3c7b2bee)
-- TODO: only on cmdheight 0?
-- FIXME: sometimes(???) crashes heirline when query is '%'
return {
  condition = function(self)
    local lines = vim.api.nvim_buf_line_count(0)
    if lines > 50000 then
      return
    end

    local query = vim.fn.getreg('/')
    if query == '' then
      return
    end

    if query:match('\\%%%d+l') then
      return
    end

    local ok, search_count = pcall(vim.fn.searchcount, { recompute = 1, maxcount = -1 })
    if not ok then
      return false
    end

    local active = vim.v.hlsearch and vim.v.hlsearch == 1 and search_count.total > 0
    if not active then
      return
    end

    self.count = search_count
    return true
  end,

  hl = {
    fg = 'search.fg',
    bg = 'search.bg',
    bold = true,
  },

  provider = function(self)
    return table.concat {
      '%{getreg("/")} [',
      self.count.current,
      '/',
      self.count.total,
      ']',
    }
  end,
}
