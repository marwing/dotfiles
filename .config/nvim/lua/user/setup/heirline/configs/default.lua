local utils = require('user.setup.heirline.utils')

local meta = utils.component('meta')
local file = utils.component('file')

local priorities = {
  lazy = 0,
  lsp = 1,
  mode = 2,
  git = 3,
  file = 4,
  obsession = 5,
  format = 6,
  enc = 6,
  wordcount = 7,
  type = 8,
  visual_selection = 9,
}

---@param priority number
---@param component table
---@return table
local function make_optional(component, priority)
  vim.validate { priority = { priority, 'number' } }

  return {
    flexible = priority,
    component,
    {},
  }
end

local statusline = {
  {
    flexible = priorities.mode,
    meta.slanted_right('mode.long'),
    meta.slanted_right('mode.short'),
  },
  meta.slanted('spell'),
  meta.slanted('search'),
  meta.slanted('macro'),
  make_optional(meta.slanted(file.file, 'stl.surround'), priorities.file),
  {
    flexible = priorities.git,
    meta.slanted('git.long'),
    meta.slanted('git.short'),
  },
  utils.component('diagnostics'),

  meta.align,
  meta.truncate,

  {
    flexible = priorities.lsp,
    utils.surround('lsp.expanded', ' ', false),
    utils.surround('lsp.long', ' ', false),
    utils.surround('lsp.short', ' ', false),
  },
  make_optional(meta.blocked('wordcount'), priorities.wordcount),
  make_optional(meta.blocked('visual_selection'), priorities.visual_selection),
  make_optional(meta.blocked('obsession'), priorities.obsession),
  meta.blocked('vimtex'),
  make_optional(meta.blocked('lazy'), priorities.lazy),
  make_optional(meta.blocked(file.type), priorities.type),
  make_optional(meta.blocked(file.format), priorities.format),
  make_optional(meta.blocked(file.enc), priorities.enc),
  meta.blocked(file.position),
}

local winbar = {
  meta.space,
  file.file,
  make_optional(utils.component('gps'), 1),

  meta.align,

  utils.component('diagnostics'),
  meta.space,
}

local tabline = utils.component('tab')

return {
  statusline = statusline,
  winbar = winbar,
  tabline = tabline,
}
