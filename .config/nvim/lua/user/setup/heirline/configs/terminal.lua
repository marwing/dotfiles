-- TODO: parse and show toggleterm, pid and cwd out of bufname
local conditions = require('heirline.conditions')

local utils = require('user.setup.heirline.utils')

local meta = utils.component('meta')

local devicons = require('nvim-web-devicons')

local function condition()
  return conditions.buffer_matches {
    buftype = { 'terminal' },
  }
end

local term = {
  icon = {
    init = function(self)
      self.icon, self.icon_color = devicons.get_icon_color('terminal')
    end,
    provider = function(self)
      return self.icon
    end,
    hl = function(self)
      return { fg = self.icon_color }
    end,
  },

  command = {
    provider = function()
      return vim.api.nvim_buf_get_name(0):gsub('.*:', '')
    end,
  },
}

local statusline = {
  condition = condition,

  meta.slanted_right('mode.long'),
  meta.align,
}

local winbar = {
  condition = condition,

  meta.space,
  term.icon,
  meta.space,
  term.command,
  meta.align,
}

return {
  statusline = statusline,
  winbar = winbar,
}
