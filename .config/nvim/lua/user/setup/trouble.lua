-- TODO: sidebar with vertical split symbols/lsp
---@module "lazy"
---@type LazySpec
return {
  {
    'folke/trouble.nvim',
    ---@module "trouble"
    ---@type trouble.Config
    opts = {
      warn_no_results = false,
      open_no_results = true,
    },
    cmd = { 'Trouble' },
    -- stylua: ignore
    keys = {
      { '<leader>xx', '<cmd>Trouble diagnostics toggle<cr>',                            desc = 'Diagnostics (Trouble)' },
      { '<leader>xX', '<cmd>Trouble diagnostics toggle filter.buf=0<cr>',               desc = 'Buffer Diagnostics (Trouble)' },
      { '<leader>xs', '<cmd>Trouble symbols toggle focus=false auto_preview=false<cr>', desc = 'Symbols (Trouble)' },
      { '<leader>xl', '<cmd>Trouble lsp toggle focus=false win.position=right<cr>',     desc = 'LSP Definitions / references / ... (Trouble)' },
      { '<leader>xL', '<cmd>Trouble loclist toggle<cr>',                                desc = 'Location List (Trouble)' },
      { '<leader>xQ', '<cmd>Trouble qflist toggle<cr>',                                 desc = 'Quickfix List (Trouble)' },
      -- { "<leader>xt", function() require("trouble").toggle("todo") end },
    },
  },
  -- missing https://github.com/folke/todo-comments.nvim/pull/220
  -- {
  --   'folke/todo-comments.nvim',
  --   dependencies = { 'nvim-lua/plenary.nvim' },
  -- },
}
